﻿// See https://aka.ms/new-console-template for more information
using jaslibdss;
using CommandLine;

namespace jaslibdss
{
    class Program
    {
        public class Options
        {
            [Option('h', "server")]
            public string? Server { get; set; }

            [Option('i', "instance")]
            public string? Instance { get; set; }

            [Option('d', "database")]
            public string? Database { get; set; }

            [Option('u', "user")]
            public string? User { get; set; }

            [Option('p', "password")]
            public string? Password { get; set; }

            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }

            [Option('c', "compare")]
            public IEnumerable<string>? Compare { get; set; }

            [Option('s', "snapshot", Required = false, HelpText = "Take a snapshot of a database.")]
            public string? Snapshot { get; set; }
        }

        static void Main(string[] args)
        {
            List<string> _comparePaths = new();
            string _snapshotPath = string.Empty;
            bool _verbose = false;

            string _server = string.Empty;
            string _instance = string.Empty;
            string _database = string.Empty;
            string _user = string.Empty;
            string _password = string.Empty;

            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed<Options>(o =>
                   {
                       if (o.Server != null)
                       {
                           _server = o.Server;
                       }

                       if (o.Instance != null)
                       {
                           _instance = o.Instance;
                       }

                       if (o.Database != null)
                       {
                           _database = o.Database;
                       }

                       if (o.User != null)
                       {
                           _user = o.User;
                       }

                       if (o.Password != null)
                       {
                           _password = o.Password;
                       }

                       _verbose = o.Verbose;

                       if (o.Compare != null)
                       {
                           _comparePaths = o.Compare.ToList<string>();
                       }

                       if (o.Snapshot != null)
                       {
                           _snapshotPath = o.Snapshot;
                       }

                   });

            libdss dss = new(_server, _instance, _database, _user, _password);

            if ((_snapshotPath != null) && (_snapshotPath != String.Empty))
            {
                dss.Snapshot(_snapshotPath, _verbose);
            }

            if ((_comparePaths.Count > 0) && (_comparePaths.Count < 3))
            {
                libdss.Compare(_comparePaths[0], _comparePaths[1], _verbose);
            }
        }
    }
}
