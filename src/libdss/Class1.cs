﻿using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using System.Reflection;

namespace jaslibdss
{
    public class libdss : ISnapshot
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private string Server { get; set; }
        private string Instance { get; set; }
        private string Database { get; set; }
        private string? User { get; set; }
        private string? Password { get; set; }
        private bool WindowsAuthentication { get; set; }

        public libdss(string server, string instance, string database,
                      string user, string password)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            Server = server;
            Instance = instance;
            Database = database;
            User = user;
            Password = password;
            WindowsAuthentication = false;
        }

        public libdss(string server, string instance,
                      string database)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            Server = server;
            Instance = instance;
            Database = database;
            User = null;
            Password = null;
            WindowsAuthentication = true;
        }

        public void Snapshot(string path, bool verbose)
        {
            log.Debug("Test log entry!");
            List<string> tables = Tables();
            SnapshotDatabase(path, tables);
            // TODO: Disconnect(connection);
        }

        private void SnapshotDatabase(string path, List<string> tables)
        {
            Parallel.ForEach(tables,
                table =>
                {
                    SnapshotTable(path, table);
                });
        }

        private void SnapshotTable(string path, string table)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string query = "SELECT *";
            query += $" FROM [{table}]";

            using (SqlConnection connection = Connect())
            {
                using (SqlCommand command = new(query, connection))
                {
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        using (DataTable dataTable = new())
                        {
                            dataTable.TableName = table;
                            dataTable.Load(dataReader);
                            string fileName = $@"{path}\{table}.xml";
                            dataTable.WriteXml(fileName);
                        }

                    }

                }

            }

        }

        private string ConnectionString()
        {
            string connectionString = $@"Data Source={Server}\{Instance};";
            connectionString += $@"Initial Catalog={Database};";
            if (!WindowsAuthentication)
            {
                connectionString += $@"User ID={User};";
                connectionString += $@"Password={Password};";
            }

            return connectionString;
        }

        private SqlConnection Connect()
        {
            string connectionString = ConnectionString();

            SqlConnection connection = new(connectionString);
            try
            {
                connection.Open();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(ex.Number);
            }

            return connection;
        }

        private List<string> Tables()
        {
            List<string> tables = new();
            SqlConnection connection = Connect();
            string query = "SELECT TABLE_NAME";
            query += $" FROM [{Database}].INFORMATION_SCHEMA.TABLES";
            query += " WHERE TABLE_TYPE = 'BASE TABLE'";
            SqlCommand command = new(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    tables.Add(reader.GetString(reader.GetOrdinal("TABLE_NAME")));
                }
            }

            return tables;
        }

        public static void Compare(string pathA, string pathB, bool verbose)
        {
            string[] pathATables = Directory.GetFiles(pathA);
            Parallel.ForEach(pathATables,
                table =>
                {
                    FileInfo tableInfo = new FileInfo(table);
                    string tableName = tableInfo.Name;
                    CompareTable(pathA, pathB, tableName, verbose).Wait();
                });
        }

        private static async Task CompareTable(string pathA, string pathB, string table, bool verbose)
        {
            string pathATable = $@"{pathA}\{table}";
            string pathBTable = $@"{pathB}\{table}";
            StreamReader streamReaderA = new(pathATable);
            StreamReader streamReaderB = new(pathBTable);

            string tableA = await streamReaderA.ReadToEndAsync();
            string tableB = await streamReaderB.ReadToEndAsync();

            if (tableA != tableB)
            {
                Console.WriteLine($"Table has differences {table}.");
            }

            if (verbose)
            {
                if (tableA == tableB)
                {
                    Console.WriteLine($"Table has not changed {table}.");
                }
            }

        }
    }
}