﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jaslibdss
{
    internal interface ISnapshot
    {
        public void Snapshot(string path, bool verbose);
    }
}
